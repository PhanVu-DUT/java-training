package javacore.string;

public class Bai2 {
    //Tính tổng của các số nguyên trong chuỗi.
    public static void main(String[] args) {
        String str = "sadf 1 sfdg s fsv 3 3.2 3,4 ds 5/6 5?9 ";

        System.out.println("result: " + sum(str));
    }

    public static int sum(String str){
        StringBuilder stringBuilder = new StringBuilder(str);
        // Replace all chacracter isn't a number to " "
        for(int i = 0; i< str.length(); i++){
            if(!Character.isDigit(str.charAt(i))){
                stringBuilder.setCharAt(i, ' ');
            }
        }

        String[] numbers = stringBuilder.toString().trim().split(" ");
        int sum = 0;
        for (int i = 0; i< numbers.length; i++){
            if(!numbers[i].equals("")) {
                sum += Integer.parseInt(numbers[i].trim());
            }
        }
        return sum;
    }
}
