package javacore.string;

public class Bai4 {
    //Tìm các từ xuất hiện trên 2 lần trong chuỗi (chuỗi đầu vào chỉ có các ký tự chữ cái và khoảng trắng)
    public static void main(String[] args) {
        String str = "Phan Van Vu Vu Vu Van Van Van Phan Phan Phan a saf asf";
        String[] words = str.split(" ");
        boolean[] flags = new boolean[words.length];

        // Đánh dấu vị trí các phần tử đã xét.
        for (int i = 0; i< words.length; i++){
            flags[i] = false;
        }
        int count;
        for(int i = 0; i< words.length; i++){
            count = 0;
            for(int j = i; j< words.length; j++){
                if(!flags[j]){
                    if (words[i].equals(words[j])){
                        count ++;
                        flags[j] = true;
                    }
                }
            }
            if(count > 2) {
                System.out.println(words[i]);
            }
        }
    }


}
