package javacore.string;

public class Bai1 {
    //Kiểm tra chuỗi ký tự có đối xứng hay không. (Ví dụ abcdcba là đối xứng)
    public static void main(String[] args) {
       String str = "absdba";
        System.out.println(kiemTraDoiXung(str));

    }

    public static boolean kiemTraDoiXung(String str){
        StringBuilder stringBuilder = new StringBuilder(str);
        return  str.equals(stringBuilder.reverse().toString());
    }

}
