package javacore.bai4;

import java.util.Scanner;

public class Bai_4 {
	public static void main(String[] args) {
		int n;
		do {
			System.out.print("\ninput n: ");
			Scanner sc = new Scanner(System.in);
			try {
				n = sc.nextInt();
				findAllFibonacis(n);
			} catch (Exception e) {
				System.out.println("Not a number !");
			}
			
			
		}while(true);
	}
	
	int find(int f1,int f2, int f3) {
		f3 = f1 + f2;
		f1 = f2;
		f2 = f3;
		return f3;
	}
	
	
	public static void findAllFibonacis(int n) {
		int f1 = 1, f2 = 1, f3;
		if(n > 1) {
			System.out.print("Result: 1 1 ");
			for(int i = 1; i< n; i++) {
				f3 = f1 + f2;
				f1 = f2;
				f2 = f3;
				if(f3 < n) {
					System.out.print(f3 + " ");
				}
			}
		}
		else System.out.println("No fibonacis less than " + n);
	}
}
