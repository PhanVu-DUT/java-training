package javacore.bai1;

public class Bai_1 {

	public static void main(String[] args) {
		int a = 4, b = 6;
		System.out.println("UCNN(" + a + "," + b + ") = " + UCNN(a, b));
		System.out.println("BCLN(" + a + "," + b + ") = " + BCLN(a, b, UCNN(a,b)));
	}
	
	public static int UCNN(int a, int b) {
		if(a == b) return a;
		if(a > b) return UCNN(a-b, a);
		else return UCNN(a, b-a);		
	}
	
	public static int BCLN(int a, int b, int ucln) {
		return (a*b) / ucln;
	}
	
}
