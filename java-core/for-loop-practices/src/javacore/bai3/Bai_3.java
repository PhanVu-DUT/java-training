package javacore.bai3;

public class Bai_3 {
	public static void main(String[] args) {
		int n = 100;
		int dem = 0;
		
		for(int i = 2; i <= n; i++) {
			dem = 0;
			while(n % i == 0) {
				dem ++;
				n /= i;
			}
			if(dem != 0) {
				System.out.println(i);
				if(n > i) System.out.println("*");
			}
		}
	}
}
