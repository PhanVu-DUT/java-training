package javacore.arrays;

public class Bai5 {
	//Viết chương trình tính tổng của các số nguyên tố trong một mảng số nguyên

	public static void main(String[] args) {
		int arrs[] = {1,2,3,4,5,5,6,7,8};
		System.out.println("Sum of primes: " + sum(arrs));
	}
	
	public static int sum(int[] arrs) {
		int sum = 0;
		for(int i = 0; i< arrs.length; i++) {
			if(isPrimeNumber(arrs[i])) {
				sum += arrs[i];
			}
		}
		return sum;
	}
	public static boolean isPrimeNumber(int n) {
		if(n < 2) return false;
		int count = 0;
		for(int i = 2; i*i <= n; i++) {
			if(n %i == 0) {
				count++;
			}
		}
		if(count == 0) return true;
		else return false;
	}
}
