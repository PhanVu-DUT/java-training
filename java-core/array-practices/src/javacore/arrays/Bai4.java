package javacore.arrays;

public class Bai4 {

	// Viết chương trình sử dụng phương pháp tìm kiếm nhị phân để tìm kiếm một số nguyên từ một mảng số nguyên đã sắp xếp (tăng dần hoặc giảm dần), trả về vị trí nếu tìm thấy hoặc -1 nếu không tìm thấy
	public static void main(String[] args) {
		int[] arrs = {1,2,3,4,5,6};
		int left = 0, right = arrs.length - 1;
		int x = 30;
		System.out.println("rs: " + findBinary(arrs, left, right, x));
		
	}
	public static int findBinary(int[] arrs, int left, int right, int x) {
		if(left <= right) {
			int mid = (left + right) / 2;
			if(arrs[mid] == x) {
				return mid;
			}
			if(arrs[mid] > x) return findBinary(arrs, mid + 1, right, x);
			return findBinary(arrs, left, mid - 1, x);
		}
		return -1;
	}
}
