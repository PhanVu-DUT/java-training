package javacore.arrays;

public class Bai1 {
	// Viết chương trình tìm những số xuất hiện trên 2 lần trong một mảng số nguyên
	public static void main(String[] args) {
		int[] arrs = {1,2,3,4,5,6,1,1,2,2,3,5,3,3};
		boolean flags[] = new boolean[arrs.length];
		for (int i = 0; i< arrs.length; i++) {
			flags[i] = false;
		}
		int count;
		for(int i = 0; i< arrs.length; i++) {
			count = 0;
			if(!flags[i]) {
				for(int j = 0; j < arrs.length; j++ ) {
					if(arrs[i] == arrs[j]) {
						count ++;
						flags[j] = true;
					}
				}
			}	
			
			if(count > 2 ) {
				System.out.println(arrs[i]);
			}
		}
	}
}
