package javacore.arrays;

public class Bai3 {
	//Viết chương trình tìm tổng của các số chẵn và trừ đi tổng các số lẻ trong một mảng số nguyên
	public static void main(String[] args) {
		int[] arrs = {1,2,3,4,5,6,7};
		int sumOfEvens = 0, sumOfOdds = 0;
		for(int i = 0; i< arrs.length; i++) {
			if(arrs[i] % 2 == 0) {
				sumOfEvens += arrs[i];
			}else sumOfOdds += arrs[i];
		}
		System.out.println("Result: " + (sumOfEvens-sumOfOdds));
	}
}
