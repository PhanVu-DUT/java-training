package javacore.arrays;

public class Bai2 {
	//Viết chương trình tìm tìm tổng của 3 số lớn nhất trong một mảng số nguyên
	final static int MAX_INT = -Integer.MAX_VALUE;
	public static void main(String[] args) {
		int[] arrs = {1,2,3,4,5,5,6,5,4};
		boolean flags[] = new boolean[arrs.length];
		
		int max, sum = 0, count = 0;
		while(count < 3) {
			int index = 0;
			max = -Integer.MAX_VALUE;
			for(int i = 0; i< arrs.length; i++) {
				if(arrs[i] > max) {
					max = arrs[i];
					index = i;
				}
			}
			sum += max;
			arrs[index] = MAX_INT;
			count ++;
		}
		
		System.out.println("sum: " + sum);
		
	}
}
